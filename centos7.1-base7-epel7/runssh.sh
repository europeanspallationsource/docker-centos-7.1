SSHPORT=${SSHPORT:-8022}
docker stop sshd
docker rm sshd
echo "Will use $SSHPORT port"
docker run --name sshd -d --privileged -p $SSHPORT:22 europeanspallationsource/docker-centos-7.1:latest
